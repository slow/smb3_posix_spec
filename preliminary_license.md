**[Note: This is the notice that would go on the final version of the specification.]**

# UNIX Extensions for SMB2 Specification License, Version 1.0

This specification for Unix Extensions for SMB2, Version 1.0 (the “Specification”)
contains contributions from the following Contributors: [List of all contributors].

Each Contributor grants the following rights and makes the following promises with
respect to the Specification, subject to the indicated limitations and other provisions:

* Each Contributor grants you the right under its copyrights to reproduce and
distribute copies of those Contributions as a part of the Specification. You are
not authorized to create any derivative works of the Specification.
* Each Contributor agrees not to assert any of its patent claims that are
necessarily infringed by an implementation of the Specification on account of
your making, using, selling, offering for sale, or importing an implementation of all
required portions of the Specification.
* Each Contributor reserves all of its rights not expressly granted above. No
additional rights other than those described above are granted by implication,
exhaustion, estoppel or otherwise.
* The licenses and promises made above do not extend to any technologies that
may be referenced by the Specification or that may be required in order to
implement the Specification.
* The Contributors do not give any assurance that any of its intellectual property
rights cover your implementation or are enforceable.
* Each Contributor may revoke its license and/or promise granted above if you or
anyone affiliated with you allege that the Contributor’s implementation of any
portion of the Specification infringes any of your or your affiliates’ patent rights.
* THE CONTRIBUTIONS AND SPECIFICATION ARE PROVIDED “AS IS” AND
THE CONTRIBUTORS DISCLAIM ALL WARRANTIES, GUARANTEES, AND
CONDITIONS, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
BUT NOT LIMITED TO ANY IMPLIED WARRANTIES OR MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
* TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THE
CONTRIBUTORS DISCLAIM LIABILITY FOR ANY INDIRECT, INCIDENTAL,
CONSEQUENTIAL, PUNITIVE OR SPECIAL DAMAGES WHATSOEVER
ARISING OUT OF OR IN ANY WAY CONNECTED TO THE USE OF OR
INABILITY TO USE THE CONTRIBUTIONS OR SPECIFICATION, OR
OTHERWISE UNDER OR IN CONNECTION WITH ANY PROVISION OF THIS
NOTICE, REGARDLESS OF THE LEGAL THEORY UPON WHICH ANY CLAIM
FOR SUCH DAMAGES IS BASED. THE FOREGOING EXCLUSION APPLIES
EVEN IF A CONTRIBUTOR HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGES IN ADVANCE AND EVEN IF ANY AVAILABLE REMEDY
FAILS OF ITS ESSENTIAL PURPOSE.
* This notice does not grant any right to use any of the names and trademarks of
any of the Contributors for any purpose.

Additional Contributor-specific notices:

* Microsoft has made available a license for its patents covering the SMB2
protocol. The terms of that license can be accessed by contacting
protocol@microsoft.com.
