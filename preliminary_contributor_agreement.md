**Contributor’s Agreement – UNIX Extensions for SMB2**

**Contributions to this project require that you grant certain rights under your
patents and copyrights to enable the development and use of the specification
being developed. The terms are as follows:**

<ol>
<li>Definitions:
<ol type='a'>
<li>“Contribution(s)” means one or more contributions to the Specification that I
submit to the Project in written form.</li>
<li>“Final Version” means any version of the Specification that is designated as final
by the Project Coordinator and is published on the Project website.</li>
<li>“Project” means the “Unix Extensions for SMB2” project at the cifs.org website or
a separate site designated by the Project coordinator.</li>
<li>“Project Coordinator” means the individual designated on the Project website as
the coordinator for the Project.</li>
<li>“Specification” means the Unix Extensions for SMB2 specification that is
developed under the Project.</li>
<li>“Contributor’s Organization” means an employer or other entity that I designate
when I register with the Project.</li>
</ol>
</li>
<li>Contributor. If I contribute to the development of the Specification on behalf of a
Contributor’s Organization, as specified herein (i) I represent that I am entering into
this agreement on behalf of that entity and its affiliates and that I have the right to do
so; and (ii) all references to “I” and “my” will apply to Contributor’s Organization.
</li>
<li>Copyright License. I grant a worldwide, non-exclusive, royalty-free license under my
copyrights to reproduce, publicly display, and distribute copies of my Contributions,
to the extent those Contributions are incorporated into preliminary drafts or Final
Version of the Specification.
</li>
<li>Patent Covenants.
<ol type='a'>
<li>Contributions. Except as described below, I promise not to assert any of my
patent claims that are necessarily infringed by an implementation of my
Contributions that are accepted for inclusion in the Final Version on account of
anyone’s making, using, selling, offering for sale, or importing an implementation
of all required portions of those Contributions.</li>
<li>Final Version. The Project Coordinator will give me at least 45 days’ notice
before publication of the Final Version, during or before which time I may give
written notice to the Project Coordinator indicating that I wish to limit my patent
promise to my Contributions as specified in Section 4(a). If I do not provide that
notice, then effective as of the date of publication of the Final Version, I promise
not to assert any of my patent claims that are necessarily infringed by an
implementation of the Final Version on account of anyone’s making, using,
selling, offering for sale, or importing an implementation of all required portions of
the Final Version.</li>
</ol>
</li>
<li>Limitations:
<ol type='a'>
<li>I reserve all rights not expressly granted in this Agreement. No additional rights
other than under my copyrights and my patent claims described in Sections 4(a)
and 4(b) are granted by implication, exhaustion, estoppel or otherwise. The
licenses and promises made above extend only to technologies that are fully
described in the Specification, and not to any technologies merely referenced in
or incorporated into the Specification, nor to enabling technologies needed to
implement the Specification.</li>
<li>I do not give any assurance that I have any intellectual property rights that cover
any implementation of the Specification, or are enforceable.</li>
</ol>
</li>
<li>Revocation of Grants. I may revoke the license and promises granted in Section 4
above with respect to any entity that alleges that my implementation of any portion of
the Specification infringes any of its patent rights. I may also revoke the license and
promise with respect to that entity if any of its affiliates makes those allegations.
</li>
<li>The Final Version will be non-confidential. All preliminary drafts of the Specification
will be maintained as confidential by the Contributors.
</li>
<li>DISCLAIMERS. MY CONTRIBUTIONS ARE PROVIDED “AS IS” AND I DISCLAIM
ALL WARRANTIES, GUARANTEES, AND CONDITIONS, WHETHER EXPRESS,
IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO ANY IMPLIED
WARRANTIES OR MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT.
</li>
<li>LIMITATIONS OF LIABILITY. EXCEPT FOR BREACHES OF CONFIDENTIALITY,
TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, I DISCLAIM
LIABILITY FOR ANY INDIRECT, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR
SPECIAL DAMAGES WHATSOEVER ARISING OUT OF OR IN ANY WAY
CONNECTED TO THE USE OF OR INABILITY TO USE MY CONTRIBUTIONS, OR
OTHERWISE UNDER OR IN CONNECTION WITH ANY PROVISION OF THIS
AGREEMENT, REGARDLESS OF THE LEGAL THEORY UPON WHICH ANY
CLAIM FOR SUCH DAMAGES IS BASED. THE FOREGOING EXCLUSION
APPLIES EVEN IF I HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH
DAMAGES IN ADVANCE AND EVEN IF ANY AVAILABLE REMEDY FAILS OF ITS
ESSENTIAL PURPOSE.
</li>
</ol>
